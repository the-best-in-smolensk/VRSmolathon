extends CharacterBody3D
class_name PlayerXR

@export var speed: float = 2.0

@onready var xr_camera_3d = %XRCamera3D
@onready var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")

var move_stick: Vector2


func _physics_process(delta):
	var dir: Vector3 = (xr_camera_3d.to_global(Vector3(move_stick.x, 0, -move_stick.y)) - xr_camera_3d.global_position) * speed
	velocity.x = dir.x
	velocity.z = dir.z
	if not is_on_floor():
		velocity.y -= gravity * delta
	move_and_slide()
	

func _on_left_hand_input_vector_2_changed(name, value):
	if name == "primary":
		move_stick = value
