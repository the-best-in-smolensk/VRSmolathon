extends XRController3D
class_name XrHand

@onready var xr_interactor: XrInteractor = $XrInteractor

var _is_pressed: bool

func _on_input_float_changed(name, value):
	if name == "grip":
		if value == 1 and !_is_pressed:
			_is_pressed = true
			xr_interactor.start_drag()
			
		if value != 1 and _is_pressed:
			_is_pressed = false
			xr_interactor.stop_drag()
