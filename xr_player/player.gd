extends Node3D
class_name Player

@export var speed: float = 2.0
@export var rotation_step_angle: float


@onready var xr_camera_3d = $XRCamera3D


var move_stick: Vector2


func _ready():
	pass


func _physics_process(delta):
	var dir: Vector3 = (xr_camera_3d.to_global(Vector3(move_stick.x, 0, -move_stick.y)) - xr_camera_3d.global_position) * delta * speed
	dir = dir.slide(Vector3.UP)
	position += dir
	

func _on_left_hand_input_vector_2_changed(name, value):
	if name == "primary":
		move_stick = value
	
	
	
