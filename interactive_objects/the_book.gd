extends RigidBody3D


@export var sphere: TeleportSphere
@export var player: Node3D
@export var player_target: Node3D
@export var play_music: AudioStreamPlayer
@export var stop_music: AudioStreamPlayer

func _on_xr_interactable_capture_updated() -> void:
	if sphere and (get_node('./XrInteractable') as XrInteractable)._draggers:
		sphere.teleport.connect(func():
				player.global_position = player_target.global_position
				if play_music:
					play_music.playing = true
				if stop_music:
					stop_music.playing = false
				)
		sphere.get_node("./AnimationPlayer").play("fade_in")
	else:
		queue_free()
	
	
