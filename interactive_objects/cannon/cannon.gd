extends Node3D
class_name Cannon


@export var move_speed: float
@export var first_stage: Node3D
@export var second_stage: Node3D
@export var barrel: NodePath
@export var ball_speed: float = 5.

@onready var cannon = %cannon
@onready var projectile_spawn = %ProjectileSpawn
@onready var ball_scene := preload("res://interactive_objects/cannon/cannon_ball.tscn")

var contains_ball: bool
var contains_powder: bool


func _on_area_3d_body_entered(body):
	if body.is_in_group("gunpowder_box") and not contains_powder:
		contains_powder = true
		_move_into_cannon(body)
	if body.is_in_group("cannon_ball") and not contains_ball and contains_powder:
		contains_ball = true
		_move_into_cannon(body)


func _move_into_cannon(body: RigidBody3D):
	body.freeze = true
	var pos = body.global_position
	var rot = body.global_rotation
	body.get_parent().remove_child(body)
	add_child(body)
	body.global_position = pos
	body.global_rotation = rot
	
	await _move_to(body, first_stage)
	await _move_to(body, second_stage)
	
	body.queue_free()


func _move_to(body: Node3D, target: Node3D):
	var progress: float = 0
	var animation_time = (body.global_position - target.global_position).length() / move_speed
	while progress < 1:
		progress += get_process_delta_time() / animation_time
		body.global_position -= (body.global_position - target.global_position).normalized() * move_speed * get_process_delta_time()
		await get_tree().process_frame
	body.global_position = target.global_position


func _on_xr_interactable_interactor_global_position_changed(pos: Vector3):
	var dir = (cannon.global_position - pos).normalized()
	var dir2 = (cannon.global_position - %Hook.global_position).normalized()
	var angle_y = dir.slide(Vector3.UP).signed_angle_to(dir2.slide(Vector3.UP), Vector3.UP)
	cannon.rotate_y(-angle_y)


func _on_barrel_interactor_global_position_changed(pos):
	var _barrel: Node3D = get_node(barrel)
	_barrel.look_at(pos)
	_barrel.rotation.y = 0
	_barrel.rotation.z = 0


func _on_shoot_trigger_area_entered(area: Area3D):
	if area.is_in_group("torch") and contains_ball and contains_powder:
		shoot()


func shoot():
	if contains_ball and contains_powder:
		for i in get_tree().get_nodes_in_group("pole_png"):
			i.fall()
		contains_ball = false
		contains_powder = false
		($cannon/Cylinder_001/ProjectileSpawn/Node3D.get_child(0) as GPUParticles3D).emitting = true
#		var ball: RigidBody3D = ball_scene.instantiate()
#		get_parent().add_child(ball)
#		global_transform
#		ball.global_transform = projectile_spawn.global_transform
#		ball.linear_velocity = Vector3.UP * ball.global_transform * ball_speed
