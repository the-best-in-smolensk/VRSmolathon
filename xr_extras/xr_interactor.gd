extends Area3D
class_name XrInteractor

var _body_to_drag: Node3D
var _dragged_body_parent: Node
var _interactable_to_send_position: XrInteractable 

var bodies_inside: Array[Node3D] = []
var _selected_body: Node3D:
	get:
		return _selected_body
	set(value):
		if _selected_body != null:
			_selected_body.get_node("XrInteractable").perform_deselect()
		_selected_body = value
		if _selected_body != null:
			_selected_body.get_node("XrInteractable").perform_select()


func start_drag():
	if _selected_body == null: return
	_body_to_drag = _selected_body
	var interactable: XrInteractable = _selected_body.get_node("XrInteractable")
	
	if interactable.drag_mode == XrInteractable.DragMode.ParentSavePose:
		var pos = _body_to_drag.global_position
		var rot = _body_to_drag.global_rotation
		_body_to_drag.freeze = true
		_dragged_body_parent = _selected_body.get_parent()
		_dragged_body_parent.remove_child(_body_to_drag)
		add_child(_body_to_drag)
		_body_to_drag.global_position = pos
		_body_to_drag.global_rotation = rot
		interactable.perform_capture()
	
	elif interactable.drag_mode == XrInteractable.DragMode.SendGlobalPosition:
		_interactable_to_send_position = interactable
	
	

func stop_drag():
	_selected_body = null
	if _body_to_drag != null:
		var interactable = _body_to_drag.get_node("XrInteractable")
		if interactable.drag_mode == XrInteractable.DragMode.ParentSavePose:
			if _body_to_drag.get_parent() != self: return
			var pos = _body_to_drag.global_position
			var rot = _body_to_drag.global_rotation
			remove_child(_body_to_drag)
			_dragged_body_parent.add_child(_body_to_drag)
			_body_to_drag.freeze = false
			_body_to_drag.global_position = pos
			_body_to_drag.global_rotation = rot
			interactable.perform_uncapture()
	_body_to_drag = null
	
	if _interactable_to_send_position != null:
		_interactable_to_send_position = null


func _on_body_entered(body):
	if !body.has_node("XrInteractable"): return
	if bodies_inside.has(body): return
	
	bodies_inside.append(body)
	
	if _body_to_drag != null: return
	
	_selected_body = body


func _on_body_exited(body):
	if !body.has_node("XrInteractable"): return
	if !bodies_inside.has(body): return
	
	bodies_inside.erase(body)
	
	if _body_to_drag != null: return
	
	_selected_body = bodies_inside[0] if len(bodies_inside) != 0 else null


func _process(delta):
	if _interactable_to_send_position != null:
		_interactable_to_send_position.on_interactor_global_position_changed(global_position)
