extends Node3D
class_name XrInteractable

signal interactor_global_position_changed(pos: Vector3)
signal capture_updated()

@export var outline_mesh: Node3D
@export var drag_mode: DragMode

var _selectors: int = 0
var _draggers: int = 0
var _last_position: Vector3
var _velocity: Vector3

func _ready():
	if not get_parent() is RigidBody3D:
		set_physics_process(false)


func _physics_process(delta):
	if _draggers:
		_velocity = (get_parent().global_position - _last_position) / delta
		_last_position = get_parent().global_position


func perform_select():
	_selectors += 1
	_changed()


func perform_deselect():
	_selectors -= 1
	_changed()


func perform_capture():
	_draggers += 1
	_changed()
	capture_updated.emit()


func perform_uncapture():
	_draggers -= 1
	_changed()
	if not _draggers and get_parent() is RigidBody3D:
		(get_parent() as RigidBody3D).linear_velocity = _velocity
	capture_updated.emit()


func _changed():
	outline_mesh.visible = _selectors != 0


func on_interactor_global_position_changed(pos: Vector3):
	interactor_global_position_changed.emit(pos)


enum DragMode{
	ParentSavePose,
	ParentResetPose,
	SendGlobalPosition
}
